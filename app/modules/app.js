"use strict";

class App {

    init() {
        this.intervalTimer();
    };

    /**
     * intervalTimer logs the current time
     * steps are second based
     *
     * @return {void}
     */
    intervalTimer() {
        let timer = setInterval(() => {
            let timeArray = this.splitTime(this.getCurrentTime());
            timeArray = this.convertSplittedTimeValuesToInteger(timeArray);

            // binary time object
            let timeBin = {
               hours:   this.getEightBitBinaryNumber(this.parseIntToBin(timeArray[0])),
               minutes: this.getEightBitBinaryNumber(this.parseIntToBin(timeArray[1])),
               seconds: this.getEightBitBinaryNumber(this.parseIntToBin(timeArray[2]))
            };

            this.render(timeBin);
        }, 1000);

        return;
    };

    /**
     * render creates the HTML
     *
     * @param timeBin
     */
    render(timeBin) {
        if (timeBin) {
            document.getElementById('binary-clock').innerText =
                timeBin.hours + ':' + timeBin.minutes + ':' + timeBin.seconds;
        }
    };

    /**
     *
     * @returns {string}
     */
    getCurrentTime() {
        let time = new Date();

        return time.getHours() +':'+ time.getMinutes() +':'+ time.getSeconds();
    };

    /**
     *
     * @param timeString
     * @returns {Array}
     */
    splitTime(timeString) {
        if (typeof timeString !== 'string') {
            throw Error("No string: ", timeString);
        }

        return timeString.split(":");
    };

    /**
     *
     * @param splittedTimeArray
     * @returns {Array}
     */
    convertSplittedTimeValuesToInteger(splittedTimeArray) {
        if (typeof splittedTimeArray !== 'object') {
            throw Error("Not an object: ", splittedTimeArray);
        }

        return splittedTimeArray.map(value => {
            return Number(value);
        });
    }

    /**
     *
     * @param num
     * @returns {string}
     */
    parseIntToBin(num) {
        if (typeof num !== 'number') {
            throw Error("No number: ", num);
        }

        if (num < 0) {
            throw Error("Number smaller than 0!");
        }

        return (num).toString(2);
    };

    /**
     *
     * @param binNum
     * @returns {string}
     */
    getEightBitBinaryNumber(binNum) {
        if (typeof binNum !== 'string') {
            throw Error("Not a string: ", binNum);
        }

        if (binNum.length === 8) {
            return binNum;
        }

        let zeros = "";
        for (let i = 0; i < (8 - binNum.length); i++) {
            zeros += "0";
        }
        return zeros + binNum;
    }

}

export default App;