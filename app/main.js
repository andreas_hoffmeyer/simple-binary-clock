/**
 * main.js
 * Bootstrpping the application
 *
 * @author Andreas Hoffmeyer
 * @copyright Andreas Hoffmeyer
 * @licence MIT
 */

"use strict";

import App from "./modules/app";

let app = new App();
app.init();