# Simple binary clock

This is just some play-around with ES2015. Feel free to adjust it.

## Installation

### Prerequisites

Pretty easy:

- Gulp must be install globally
- So you need node.js


    $ git clone git@bitbucket.org:andreas_hoffmeyer/simple-binary-clock.git

    $ cd simple-binary-clock

    $ npm install
    
    $ gulp    

Then just open the index.html file in the public folder.

## Licence

MIT
