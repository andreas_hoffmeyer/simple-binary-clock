// Why I'm using laravel-elixir?
// Look at the code below. That's why!
var elixir = require('laravel-elixir');

elixir(function(mix) {
    mix.browserify(
        './app/main.js',
        './public/js/main.js'
    );
});